# Mastodon to Twitter bot

This gem uses the Mastodon stream API to replicate your toot on Twitter, handling the "500 to 140 characters limit" conversion.

I advice you to use [rbenv](https://github.com/rbenv/rbenv/) to manage your Ruby environment to not mess up your system.

To use it:

 * Clone this repository somewhere (`git clone https://git.imirhil.fr/aeris/cross-post/`)
 * Install dependencies with Bundler (`bundler install`)
 * Create a `$HOME/.config/cross-post/config.yml` configuration file, based on the example available [here](https://git.imirhil.fr/aeris/cross-post/src/master/config.yml) 
 * Register the app on Twitter (`bundle exec bin/twitter-register`)
	* You can reuse my Twitter app OAuth credentials, or register a new app from scratch [here](https://apps.twitter.com/)
 * Register the app on Mastodon (`bundle exec bin/mastodon-register`)
 	* You need to change the consumer key and secret to whaveter your instance have for this app. See with your instance admin for real values.
 	* If your instance currently doesn't support this app, just remove the consumer part, registration process will create a new app on your instance.
 * Start the replication (`bundle exec bin/feed-twitter`)
 * Enjoy
 
If needed, a SystemD unit example is available [here](https://git.imirhil.fr/aeris/cross-post/src/master/mastodon-twitter.service)

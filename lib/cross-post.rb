require 'cross-post/config'
require 'cross-post/mastodon'
require 'cross-post/twitter'
require 'open-uri'
require 'logger'

# Force OpenURI#open to return a TempFile and not a StringIO
OpenURI::Buffer.send :remove_const, 'StringMax'
OpenURI::Buffer.const_set 'StringMax', 0


class CrossPost
	LOGGER = Logger.new STDERR
	LOGGER.level = Logger.const_get ENV.fetch('LOG', 'INFO').upcase
	LOGGER.formatter = proc do |severity, time, _, msg|
		time = time.strftime '%Y-%m-%dT%H:%M:%S.%6N'.freeze
		"#{time} #{severity} #{msg}\n"
	end

	attr_reader :mastodon, :twitter

	def initialize
		@config   = Config.new
		@mastodon = Mastodon.new @config
		@twitter  = Twitter.new @config
	end

	def feed
		@mastodon.feed @twitter
	end

	def self.feed
		self.new.feed
	end
end

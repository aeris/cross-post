require 'mastodon'
require 'sanitize'
require 'awesome_print'

class CrossPost
	class Mastodon
		def initialize(config)
			settings = config[:settings]
			@posts   = config[:posts]

			url   = settings['mastodon.url']
			token = settings['mastodon.token']
			user  = settings['mastodon.user']

			LOGGER.debug "Mastodon base URL: #{url}"
			@client = ::Mastodon::REST::Client.new base_url: url, bearer_token: token

			stream_url = settings.fetch 'mastodon.stream_url', url
			LOGGER.debug "Mastodon stream URL: #{stream_url}"
			@stream = ::Mastodon::Streaming::Client.new base_url: stream_url, bearer_token: token

			@user_url = URI.join(ENV.fetch('BASE_USER_URL', url), "/@#{user}").to_s
			LOGGER.debug "Mastodon user URL: #{@user_url}"
		end

		def feed(twitter)
			@stream.user do |object|
				begin
					case object
					when ::Mastodon::Status
						LOGGER.info { 'Receiving status' }
						LOGGER.debug { object.ai }
						next if reject? object
						twitter.post_status object
					end
				rescue => e
					LOGGER.error e
					raise
				end
			end
		end

		private

		def reject?(status)
			return true if status.account.url != @user_url or
					status.visibility != 'public'
			reply = status.in_reply_to_id
			return true if reply and !@posts[reply]
			false
		end
	end
end

require 'yaml'
require 'fileutils'

class CrossPost
	class Config
		DEFAULT_CONFIG_FOLDER = File.join Dir.home, '.config/cross-post'
		DEFAULT_CONFIG_FILE   = 'config.yml'

		class SubConfig
			def initialize(config = {})
				@config = config
			end

			def each(&block)
				@config.each &block
			end

			def [](key)
				case key
				when String
					current = @config
					key.split(/\./).each do |k|
						current = current[k]
						return nil if current.nil?
					end
					current
				else
					@config[key]
				end
			end

			def fetch(key, default = nil)
				self[key] || default
			end

			def []=(key, value)
				case key
				when String
					*key, last = key.to_s.split(/\./)
					current    = @config
					key.each do |k|
						next_ = current[k]
						case next_
						when nil
							next_ = current[k] = {}
						when Hash
						else
							raise "Invalid entry, Hash expected, had #{next_.class} (#{next_})"
						end
						current = next_
					end
					current[last] = value
				else
					@config[key] = value
				end
			end
		end

		class FifoSubConfig < SubConfig
			def initialize(size = 100)
				@size = size
				@keys = []
				super({})
			end

			def []=(key, value)
				@keys.delete key
				value = super key, value
				@keys << key
				while @keys.size > @size
					key = @keys.delete_at 0
					@config.delete key
				end
				value
			end
		end

		class FileSubConfig < SubConfig
			def initialize(file)
				@file = file
				super YAML.load_file @file
			end

			def put(key, value, save: false)
				self[key] = value
				self.save if save
			end

			def save
				LOGGER.debug "Saving #{@file}"
				yaml = YAML.dump @config
				File.write @file, yaml
			end
		end

		def initialize
			@configs = {}
			@dir     = ENV.fetch 'CONFIG_FOLDER', DEFAULT_CONFIG_FOLDER
			file     = ENV.fetch 'CONFIG_FILE', DEFAULT_CONFIG_FILE
			self.load :settings, file
			self.load :users
			self[:posts] = FifoSubConfig.new
		end

		def [](name)
			@configs[name]
		end

		def []=(name, value)
			@configs[name] = value
		end

		def load(name, file = nil)
			file ||= "#{name}.yml"
			file = File.join @dir, file
			File.write(file, YAML.dump({})) unless File.exist? file
			self[name] = FileSubConfig.new file
		end
	end
end

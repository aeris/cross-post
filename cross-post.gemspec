lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'cross-post/version'

Gem::Specification.new do |spec|
	spec.name = 'cross-post'
	spec.version = CrossPost::VERSION
	spec.authors = ['aeris']
	spec.email = ['aeris@imirhil.fr']
	spec.summary = 'Cross post to Mastodon and Twitter'
	spec.homepage = 'https://git.imirhil.fr/aeris/cross-post/'
	spec.license = 'AGPL-3.0+'

	spec.files = `git ls-files -z`.split("\x0")
	spec.executables = spec.files.grep(%r{^bin/}) { |f| File.basename f }
	spec.test_files = spec.files.grep %r{^(test|spec|features)/}

	spec.add_development_dependency 'bundler', '~> 1.15', '>= 1.15.4'
	spec.add_development_dependency 'rspec', '~> 3.6.0', '>= 3.6.0'

	spec.add_dependency 'twitter', '~> 6.1', '>= 6.1.0'
	spec.add_dependency 'mastodon-api', '~> 1.1', '>= 1.1.0'
	spec.add_dependency 'twitter-text', '~> 1.14', '>= 1.14.7'
	spec.add_dependency 'oauth', '~> 0.5', '>= 0.5.3'
	spec.add_dependency 'oauth2', '~> 1.4', '>= 1.4.0'
	spec.add_dependency 'launchy', '~> 2.4', '>= 2.4.3'
	spec.add_dependency 'sanitize', '~> 4.5', '>= 4.5.0'
end

require 'cross-post'

RSpec.describe CrossPost::Config::FifoSubConfig do
	it 'must remove first value in case of overflow' do
		config = CrossPost::Config::FifoSubConfig.new 2
		config[:foo] = :foo
		config[:bar] = :bar

		expect(config[:foo]).to be :foo
		expect(config[:bar]).to be :bar
		expect(config[:baz]).to be_nil

		config[:baz] = :baz

		expect(config[:foo]).to be_nil
		expect(config[:bar]).to be :bar
		expect(config[:baz]).to be :baz
	end
end
